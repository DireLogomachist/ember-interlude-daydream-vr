﻿//SHADERLAB code - custom Unity
Shader "BasicShaders/flatColor" {
	Properties {
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}
	Subshader {
		Pass {
			CGPROGRAM //Start CG (Unity) code
				
			//pragmas
			#pragma vertex vert
			#pragma fragment frag
			
			//user-defined variables
			uniform float4 _Color;
			
			//basic input structs
			struct vertexInput {
				float4 vertex : POSITION;
			
			};
			struct vertexOutput {
				float4 pos : SV_POSITION;
			};
			
			//vertex function
			vertexOutput vert(vertexInput v) {
				vertexOutput o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				
				return o;
			}
			
			//fragment function
			float4 frag(vertexOutput i) : COLOR {
				return _Color;
			}
			
			ENDCG
		}
	}
	//Uncomment for final production - commented during development
	//FALLBACK "DIFFUSE"
}

//OR...
//Shader "Solid Color" {
	//Properties { _Color ("Color", Color) = (1,1,1) }
	//SubShader { Color [_Color] Pass {} }
//} 




