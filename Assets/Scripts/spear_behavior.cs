﻿using UnityEngine;
using System.Collections;

public class spear_behavior : MonoBehaviour {
   
    public float throwStrength = 500;
	public float throwVelocity = 70;
    public float spinStrength = 1;
	public float gravityWait = 0.75f;

	[HideInInspector]
    public bool thrown = false;
	[HideInInspector]
	public bool hit = false;

    new Rigidbody rigidbody;
    Transform direction;

	void Start () {
	    rigidbody = gameObject.GetComponent<Rigidbody>();
        direction = gameObject.transform;
	}
	
	void Update () {
		//Debug.Log(rigidbody.velocity.magnitude);
		if(thrown && rigidbody.velocity.magnitude > 15) {
			//trajectory arc rotation
			transform.rotation = Quaternion.LookRotation(rigidbody.velocity);
		}
	}

	void OnCollisionEnter(Collision c) {
		hit = true;
		rigidbody.useGravity = true;
	}

    public void fireSpear() {
		thrown = true;
        rigidbody.isKinematic = false;
		//rigidbody.AddForce(direction.forward * throwStrength, ForceMode.Impulse);
		rigidbody.AddForce(direction.forward * throwVelocity, ForceMode.VelocityChange);
        rigidbody.AddTorque(direction.forward * spinStrength, ForceMode.Impulse);
		//StartCoroutine(waitForGravity());
    }

	IEnumerator waitForGravity() {
		yield return new WaitForSeconds(gravityWait);
		rigidbody.useGravity = true;
		//Debug.Log("resuming gravity");
	}

	
}
