﻿using UnityEngine;
using System.Collections;
using UnityEngine.Serialization;

public class struct_joint : MonoBehaviour {
	public float flashLength = 0.5f;

	Renderer rend;
	Color currentColor;

	public ConfigurableJoint up;
	public ConfigurableJoint down;
	public ConfigurableJoint right;
	public ConfigurableJoint left;
	public ConfigurableJoint forward;
	public ConfigurableJoint back;

	[HideInInspector] public ConfigurableJoint[] allJoints;

	public Vector3 indexLoc;

	void Awake() {
		rend = gameObject.GetComponent<Renderer>();
		currentColor = rend.material.color;

		float t = Time.realtimeSinceStartup;

		ConfigurableJoint defaultJoint = gameObject.GetComponent<ConfigurableJoint>();
		utilities.CopyComponent(defaultJoint, gameObject);
		utilities.CopyComponent(defaultJoint, gameObject);
		utilities.CopyComponent(defaultJoint, gameObject);
		utilities.CopyComponent(defaultJoint, gameObject);
		utilities.CopyComponent(defaultJoint, gameObject);

		allJoints = gameObject.GetComponents<ConfigurableJoint>();
		up = allJoints[0];
		down = allJoints[1];
		right = allJoints[2];
		left = allJoints[3];
		forward = allJoints[4];
		back = allJoints[5];

		indexLoc = new Vector3(-1,-1,-1);
	}

	void Start() {}
	void Update() {
		selfDestruct();
	}

	void OnCollisionEnter(Collision c) {
		if(c.collider.tag == "Projectile") {
			//Debug.Log("Spear hit");
			Destroy(gameObject);
			StartCoroutine(colorChange());
		}
	}

    IEnumerator colorChange() {
		rend.material.color = Color.red;
		yield return new WaitForSeconds(flashLength);
		rend.material.color = currentColor;
	}

	public void setIndex(Vector3 i) {
		indexLoc = i;
	}

	public void selfDestruct() {
		for(int i = 0; i < 6; i++) {
			if(allJoints[i] != null && !allJoints[i].Equals(null))
				allJoints[i].connectedBody.gameObject.layer = LayerMask.NameToLayer("Default");
				Destroy(allJoints[i]);
		}
		Destroy(gameObject);
	}

	public void selfDestruct(float delay) {

	}
}
