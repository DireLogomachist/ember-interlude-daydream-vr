﻿using UnityEngine;
using System.Collections;

public class light_controller : MonoBehaviour {

	public Light mainLight;
	public Light light2;
	public Light light3;

	public float delay = 2.0f;
	public float lightTimer = 1.0f;
	
	void Update () {
		//lerp the light intensity up to 1 over time
		mainLight.GetComponent<Light>().intensity = Mathf.Lerp(0.0f,1.0f,Time.time/lightTimer);
		light2.GetComponent<Light>().intensity = Mathf.Lerp(0.0f,0.2f,Time.time/lightTimer);
		light3.GetComponent<Light>().intensity = Mathf.Lerp(0.0f,1.0f,Time.time/lightTimer);
	}
}
