﻿using UnityEngine;
using System.Collections;

public class emb_int_sound_controller : MonoBehaviour {

	public GameObject[] audioSources;
	public float[] delays;

	void Start () {
		for(int i=0; i < audioSources.Length; i++) {
			audioSources[i].GetComponent<AudioSource>().PlayDelayed(delays[i]);
		}
	}
}
